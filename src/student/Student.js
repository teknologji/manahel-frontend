import React, { useState, useEffect } from "react";
import postData from "../helpers/postData";
import getData from "../helpers/getData";
import deleteData from '../helpers/deleteData';
import { useSelector } from "react-redux";
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import './style.css';
import StudentRow from '../components/StudentRow';

const Student = () => {

    const lng = useSelector(state => state.LanguageReducer.slctdLng);
    const defaultSets = useSelector(state => state.DfltSts);

    const [status, setStatus] = useState(lng.createTxt);
    const [name, setName] = useState(null);
    const [adrs, setAdrs] = useState(null);
    const [phone, setPhone] = useState(null);
    const [openAlert, setOpenAlert] = useState(false);
    const [alertMsg, setAlertMsg] = useState(null);
    const [alertSeverity, setAlertSeverity] = useState(null);
    const [students, setStudents] = useState([]);

    const handleSubmit = (e) => {
        e.preventDefault();
        setStatus(lng.sendingTxt);

        postData(defaultSets.apiUrl + 'students', { //defaultSets.apiUrl + 
            name: name,
            adrs: adrs,
            phone: phone,
        })
            .then((response) => {
                setAlertSeverity("success");
                setAlertMsg(response.data.msg);
                setOpenAlert(true);
                fetchStudents(true);

            })
            .catch((error) => {

                if (error.response !== undefined && error.response.status === 422) {
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = errorTxt + ' ' + errorMessage[key][0] + "\n";
                    });

                    setAlertSeverity("error");
                    setOpenAlert(true);
                    setAlertMsg(errorTxt);
                }
            });

        setStatus(lng.createTxt);

    };

    useEffect(() => {
        let isComponentMounted = true;

        fetchStudents(isComponentMounted);

        return () => { isComponentMounted = false; };
    }, []);

    const fetchStudents = (isComponentMounted) => {

        getData(defaultSets.apiUrl + 'students', null, {})
            .then(response => {
                if (isComponentMounted) {

                    setStudents(response.data);
                }
                //setLoading(false);
                console.log(response.data)
            })
            .catch((err) => {
                console.error(err.response.data);
            });
    }

    const handleCloseAlert = () => {
        setOpenAlert(false);
    }


    const deleteRow = (id) => {
        
        deleteData(defaultSets.apiUrl + 'students/' + id, null) //userInfo.accessToken
            .then(res => {
                const filteredData = students.filter(item => item.id !== id);
                setStudents(filteredData);
                setAlertSeverity("info");
                setOpenAlert(true);
                setAlertMsg(res.data.msg);
            })
            .catch(err => {
                console.log(err);
            });
    }

    return (
        <div className="containerDiv">
            <div id="createForm">

                <Snackbar
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                    open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                    <Alert onClose={handleCloseAlert} severity={alertSeverity} sx={{ width: '100%' }}>
                        {alertMsg}
                    </Alert>
                </Snackbar>

                <h2 id="headerTitle">{lng.crtNwStudentTxt}</h2>

                <div className="row">
                    <label>{lng.fullNameTxt}</label>
                    <input type="text"
                        name="name"
                        onChange={(e) => setName(e.target.value)}
                        placeholder={lng.entrYrNameTxt} />

                </div>

                <div className="row">
                    <label>{lng.adrsTxt}</label>
                    <input type="text"
                        onChange={(e) => setAdrs(e.target.value)}
                        placeholder={lng.entrYrAdrsTxt} />
                </div>

                <div className="row">
                    <label>{lng.phoneTxt}</label>
                    <input type="text"
                        onChange={(e) => setPhone(e.target.value)}
                        placeholder={lng.entrYrPhoneTxt} />
                </div>


                <div id="button" className="row">
                    <button onClick={handleSubmit}>{status}</button>
                </div>

                {/* <OtherMethods /> */}
            </div>


            <div id="tableList">
                <h2 id="headerTitle">{lng.studentsListTxt}</h2>
                <div className="grid-container" >
                    <div className="grid-title">{lng.fullNameTxt}</div>
                    <div className="grid-title">{lng.phoneTxt}</div>
                    <div className="grid-title">{lng.adrsTxt}</div>
                    <div className="grid-title">{lng.actionTxt}</div>
                </div>

                {students ?
                    students.map((item, i) => {

                        return (
                            <StudentRow item={item} lng={lng} deleteRow={deleteRow} key={i.toString()} />
                        )
                    })
                    :
                    <p>{lng.loadingTxt}</p>
                }

            </div>

        </div>

    );


    const OtherMethods = props => (
        <div id="alternativeLogin">
            <label>Or Register with:</label>
            <div id="iconGroup">
                <Facebook />
                <Twitter />
                <Google />
            </div>
        </div>
    );

    const Facebook = props => (
        <a href="#" id="facebookIcon"></a>
    );

    const Twitter = props => (
        <a href="#" id="twitterIcon"></a>
    );

    const Google = props => (
        <a href="#" id="googleIcon"></a>
    );

};
export default Student;