import axios from 'axios';

export default function deleteData(url, accessToken) {

  return axios.delete(url, {
    headers: {
      'Authorization': 'Bearer ' + accessToken
    }
  }
  )

}

