
import { combineReducers, createStore } from 'redux';
import LanguageReducer from './reducers/LanguageReducer';
//import Curr from '../reducers/Curr';
import DfltSts from './reducers/DfltSts';

const store = createStore(combineReducers({
    LanguageReducer,
    //Curr,
    DfltSts,
  })
  )
//console.log(store.getState())

export default store 