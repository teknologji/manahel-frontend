import { combineReducers } from 'redux'

import LanguageReducer from './LanguageReducer';
import DfltSts from './DfltSts';

export default combineReducers({
    
    LanguageReducer,
    DfltSts
});