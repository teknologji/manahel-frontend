
import { en } from '../../lang/en';
import { tr } from '../../lang/tr';

const INITIAL_STATE = {slctdLng: en};
export const LanguageReducer  = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'language_data':
            return {
                slctdLng: action.payload
            };
        default:
            return state;
    }
};

export default LanguageReducer;
