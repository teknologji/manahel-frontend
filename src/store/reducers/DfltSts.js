const INITIAL_STATE = {
  mainUrl: 'http://localhost:8000/',  
  apiUrl: 'http://localhost:8000/api/'
};


/* const INITIAL_STATE = {
  mainUrl: 'http://darselam.com/',  
  apiUrl: 'http://darselam.com/api/' 
}; */
  
export const DfltSts  = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case 'SAVE_SETTINGS':
      return action.payload;
    default:  
     return state;
  }
};

export default DfltSts;