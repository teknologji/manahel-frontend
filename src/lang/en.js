export const en = {
  crtNwStudentTxt: 'Create Student',
  entrYrNameTxt: 'Enter Your Name',
  entrYrAdrsTxt: 'Enter Your Adrs',
  entrYrPhoneTxt: 'Enter Your Phone',
  entrYrPassTxt: 'Enter Your Password',
  createTxt: 'Create',
  fullNameTxt: 'Full Name',
  adrsTxt: 'Adrs',
  phoneTxt: 'Phone',
  passwordTxt: 'Password',
  sendingTxt: 'Sending ...',
  studentsListTxt: 'Students List',
  actionTxt: 'Action',
  deleteTxt: 'Delete',
  loadingTxt: 'Loading ...'
}

