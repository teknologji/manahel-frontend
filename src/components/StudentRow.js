import React, { useEffect } from 'react';
import Button from '@mui/material/Button';



const StudentRow = ({ lng, item, deleteRow }) => {


  useEffect(() => {

    return () => { };

  }, []);


  return (
    <div className="grid-container" key={item.id}>
      <div className="grid-item">{item.name}</div>
      <div className="grid-item">{item.phone}</div>
      <div className="grid-item">{item.adrs}</div>
      <div className="grid-item">
        <Button onClick={() => deleteRow(item.id)}>
        {lng.deleteTxt}
        </Button>
      </div>
    </div>

  );
}

export default StudentRow;


