import Student from './student/Student';

import { Provider } from 'react-redux';
import store from './store/index';

function App() {
  return (
    <Provider store={store}>
    <div className="App">
      <Student />
    </div>
    </Provider>
    
  );
}

export default App;
